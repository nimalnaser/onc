## Octane number calculator (ONC)

A Python code to estimate octane numbers and octane index based on the methodology described in [1] is provided. The code is compatible with both versions of Python, 2.x and 3.x. However, the following modules are required for successful operation:

1. NumPy: a library for Python programming to perform scientific computing and technical computing; available at [https://www.numpy.org/](https://www.numpy.org/ "https://www.numpy.org/")
2. SciPy: a library for Python programming to perform scientific computing and technical computing; available at [https://www.scipy.org/](https://www.scipy.org/ "https://www.scipy.org/")

**References**

[1] Naser, N., Sarathy, S. M., & Chung, S. H. (2018). Estimating fuel octane numbers from homogeneous gas-phase ignition delay times. Combustion and Flame, 188, 307-323. doi:[10.1016/j.combustflame.2017.09.037](https://doi.org/10.1016/j.combustflame.2017.09.037 "10.1016/j.combustflame.2017.09.037").