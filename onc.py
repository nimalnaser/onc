#====================================================================================
# Octane number calculator
#====================================================================================
#
# Author            : Nimal Naser (nimal.naser@kaust.edu.sa, nimal.naser@gmail.com)
# Institute         : King Abdullah University of Science and Technology (KAUST)
# Department        : Mechanical Engineering
# Research center   : Clean Combustion Research Center
# Version           : 1.0.0
# Created on        : 29 December 2015
# Last modified on  : 02 January 2018
#
#------------------------------------------------------------------------------------
#
#------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------
# Import module
#------------------------------------------------------------------------------------
from __future__ import division


#------------------------------------------------------------------------------------
# Begin timer
#------------------------------------------------------------------------------------
import timeit
time_start = timeit.default_timer()


#------------------------------------------------------------------------------------
# Compatibility for Python 2.x and 3.x
#------------------------------------------------------------------------------------
try:
    input = raw_input
except NameError:
    pass


#------------------------------------------------------------------------------------
# Import modules and functions
#------------------------------------------------------------------------------------
import os
import logging
import time
import numpy as np
import sys
import scipy.interpolate as interp


#------------------------------------------------------------------------------------
# Log file
#------------------------------------------------------------------------------------
if not os.path.isdir('./Logs'):
    os.makedirs('./Logs')
logging.basicConfig(filename = './Logs/ONC - Log - ' + \
    str(time.strftime('%Y-%m-%d %H:%M:%S')) + '.log', level = logging.DEBUG, \
    filemode = 'w', format = '%(asctime)s\t%(levelname)s\t%(message)s\t', \
    datefmt = '[%Y-%m-%d %H:%M:%S]')
logging.info(u'\t\u2714\t\u229D Running code.')


#------------------------------------------------------------------------------------
# Correlation pressure, temperature, exponent and coefficients
#------------------------------------------------------------------------------------
coefficients_ron = np.array([
    (10, 730, -0.99, -296.01, 106.11),
    (15, 740, -1.04, -234.57, 106.60),
    (20, 750, -1.11, -179.68, 106.62),
    (25, 760, -1.19, -132.07, 106.10),
    (30, 770, -1.23, -95.91, 105.58),
    (35, 780, -1.21, -73.28, 105.64),
    (40, 790, -1.14, -61.85, 106.59),
    (45, 810, -1.07, -56.32, 108.05),
    (50, 820, -1.02, -51.65, 109.45)],
    dtype=[('pressure','f'),('temperature','f'),('n','f'),('a','f'),('b','f')])
coefficients_mon = np.array([
    (10, 780, -0.85, -206.26, 97.68),
    (15, 800, -0.88, -138.00, 97.83),
    (20, 820, -0.87, -101.85, 98.45),
    (25, 840, -0.88, -85.05, 99.32),
    (30, 860, -0.90, -74.92, 100.29),
    (35, 880, -0.93, -67.81, 101.47),
    (40, 900, -0.98, -61.22, 102.59),
    (45, 920, -1.10, -53.35, 103.15),
    (50, 940, -1.29, -43.19, 103.10)],
    dtype=[('pressure','f'),('temperature','f'),('n','f'),('a','f'),('b','f')])
coefficients_aki = np.array([
    (10, 750, -0.90, -230.34, 103.04),
    (15, 770, -0.92, -150.67, 103.25),
    (20, 790, -0.91, -108.84, 103.74),
    (25, 800, -0.93, -86.87, 103.87),
    (30, 820, -0.90, -70.81, 104.67),
    (35, 830, -0.90, -60.36, 105.01),
    (40, 850, -0.86, -54.13, 106.33),
    (45, 870, -0.83, -50.78, 108.22),
    (50, 880, -0.84, -46.05, 108.86)],
    dtype=[('pressure','f'),('temperature','f'),('n','f'),('a','f'),('b','f')])
coefficients_k_negative_half = np.array([
    (10, 710, -1.06, -482.06, 112.39),
    (15, 710, -1.28, -558.90, 111.97),
    (20, 720, -1.35, -420.47, 111.94),
    (25, 720, -1.49, -452.25, 111.79),
    (30, 730, -1.48, -308.31, 112.06),
    (35, 730, -1.58, -316.35, 111.98),
    (40, 740, -1.56, -217.71, 112.11),
    (45, 750, -1.52, -151.98, 112.45),
    (50, 760, -1.48, -109.87, 112.76)],
    dtype=[('pressure','f'),('temperature','f'),('n','f'),('a','f'),('b','f')])
coefficients_k_negative_one = np.array([
    (45, 700, -1.83, -1340.68, 117.56),
    (50, 710, -1.80, -800.67, 117.73)],
    dtype=[('pressure','f'),('temperature','f'),('n','f'),('a','f'),('b','f')])


#------------------------------------------------------------------------------------
# Obtain homogeneous batch reactor conditions
#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
# Obtain k value
#------------------------------------------------------------------------------------
restart='y'
while restart not in ('n'):
    logging.info(u'\t\u2714\t\t\u25CF Obtain k value.')
    while True:
        condition = input('Enter k value (=0 for RON-like; ' + \
            '=1 for MON-like; =0.5 for AKI-like): ')
        try:
            val = float(condition)
            logging.info(u'\t\u2714\t\t\t\u25C6 k input a numerical value.')
            if float(condition) >= -2 and float(condition) <= 2:
                logging.info(u'\t\u2714\t\t\t\t\u27A4 k input in the range of ' + \
                    u'\u22122-2.')
                break
            else:
                print u'Please enter k in the range of \u22122-2.'.encode('utf-8')
                logging.warning(u'\t\u2716\t\t\t\t\u27A4 k input not in the ' + \
                    u'range of \u22122-2.')
        except ValueError:
            print 'Please enter a numerical value.'
            logging.warning(u'\t\u2716\t\t\t\u25C6 k input not a numerical value.')
    condition = float(condition)
    logging.info(u'\t\u2714\t\t\u25CF Obtained k value input.')


#------------------------------------------------------------------------------------
# Assign coefficients based on k value
#------------------------------------------------------------------------------------
    pressure_minimum = 10
    pressure_maximum = 50
    interpolation_condition = 0
    if condition == 0:
        coefficients = coefficients_ron
        print '--------------------\n RON-like condition\n--------------------'
        result_output = 'Predicted RON: '
        logging.info(u'\t\u2714\t\t\u25CF Assigned exponents and coefficients ' + \
            'for RON-like condition.')
    elif condition == 1:
        coefficients = coefficients_mon
        print '--------------------\n MON-like condition\n--------------------'
        result_output = 'Predicted MON: '
        logging.info(u'\t\u2714\t\t\u25CF Assigned exponents and coefficients ' + \
            'for MON-like condition.')
    elif condition == 0.5:
        coefficients = coefficients_aki
        print '--------------------\n AKI-like condition\n--------------------'
        result_output = 'Predicted AKI: '   
        logging.info(u'\t\u2714\t\t\u25CF Assigned exponents and coefficients ' + \
            'for AKI-like condition.') 
    elif condition == -0.5:
        coefficients = coefficients_k_negative_half
        print u'--------------------\n k = \u22120.5 '.encode('utf-8') + \
            'condition\n--------------------'
        result_output = u'Predicted OI (k = \u22120.5): '.encode('utf-8')
        logging.info(u'\t\u2714\t\t\u25CF Assigned exponents and coefficients ' + \
            u'for k = \u22120.5 condition.')
    elif condition == -1:
        coefficients = coefficients_k_negative_one
        print u'------------------\n k = \u22121 condition\n'.encode('utf-8') + \
        '------------------'
        pressure_minimum = 45
        pressure_maximum = 50
        result_output = u'Predicted OI (k = \u22121): '.encode('utf-8')
        logging.info(u'\t\u2714\t\t\u25CF Assigned exponents and coefficients ' + \
            u'for k = \u22121 condition.')
    else:
        print u'----------------' + '-' * len(str(condition)) + '\n k = ' + \
            str(condition) + ' condition\n----------------' + \
            '-' * len(str(condition))
        interpolation_condition = 1
        result_output = u'Predicted OI (k = ' + str(condition) + '): '
        logging.info(u'\t\u2714\t\t\u25CF No exponents and coefficients ' + \
            'available for k = ' + str(condition) + ' condition. ' + \
            'Interpolation required.')


#------------------------------------------------------------------------------------
# Obtain pressure
#------------------------------------------------------------------------------------
    logging.info(u'\t\u2714\t\t\u25CF Obtain pressure.')
    interpolation_pressure = 0
    while True:
        pressure = input('Enter pressure (in bar): ')
        try:
            val = float(pressure)
            logging.info(u'\t\u2714\t\t\t\u25C6 Pressure input a numerical value.')
            if val >= pressure_minimum and val <= pressure_maximum:
                logging.info(u'\t\u2714\t\t\t\t\u27A4 Pressure input in the ' + \
                    'range of ' + str(pressure_minimum) + '-' + \
                    str (pressure_maximum) + ' bar.')
                break
            else:
                logging.warning(u'\t\u2716\t\t\t\t\u27A4 Pressure input ' + \
                    'not in the range of ' + str(pressure_minimum) + '-' + \
                    str (pressure_maximum) + ' bar.')
                print 'Please enter pressure in the range of ' + \
                str(pressure_minimum) + '-' + str (pressure_maximum) + ' bar.'
        except ValueError:
            logging.warning(u'\t\u2716\t\t\t\u25C6 Pressure input not a ' + \
                'numerical value.')
            print 'Please enter a numerical value.'
    logging.info(u'\t\u2714\t\t\u25CF Obtained pressure input.')
    pressure = float(pressure)
    for i in range(len(coefficients_ron)):
        if coefficients_ron[i][0] == pressure:
            interpolation_pressure = 0
            break
        else:
            interpolation_pressure = 1
    if interpolation_pressure == 1:    
        logging.info(u'\t\u2714\t\t\u25CF No exponents and coefficients ' + \
            'available for P = ' + str(pressure) + ' bar. Interpolation required.')


#------------------------------------------------------------------------------------
# Obtain correlation parameters based on input
#------------------------------------------------------------------------------------
    if interpolation_condition != 1 and interpolation_pressure != 1:
        logging.info(u'\t\u2714\t\t\u25CF Scan for correlation exponents and ' + \
            'coefficients based on k value and pressure.')  
        for i in range(len(coefficients)):
            if coefficients[i][0] == pressure:
                break
        temperature = coefficients[i][1]
        n = coefficients[i][2]
        a = coefficients[i][3]
        b = coefficients[i][4]
        logging.info(u'\t\u2714\t\t\u25CF Obtained exponent and coefficients ' + \
            'for correlation.') 
    else:
        if pressure < 45:
            logging.info(u'\t\u2714\t\t\u25CF Performing interpolation for ' + \
                'exponent and coefficients.')
            x_pressure = coefficients_ron['pressure']
            y_condition = np.array([1.0,0.5,0.0,-0.5])
            xx, yy = np.meshgrid(x_pressure,y_condition)
            z_n = np.vstack((coefficients_mon['n'],coefficients_aki['n'], \
                coefficients_ron['n'],coefficients_k_negative_half['n']))
            z_a = np.vstack((coefficients_mon['a'],coefficients_aki['a'], \
                coefficients_ron['a'],coefficients_k_negative_half['a']))
            z_b = np.vstack((coefficients_mon['b'],coefficients_aki['b'], \
                coefficients_ron['b'],coefficients_k_negative_half['b']))
            z_temperature = np.vstack((coefficients_mon['temperature'], \
                coefficients_aki['temperature'],coefficients_ron['temperature'], \
                coefficients_k_negative_half['temperature']))        
            f_n = interp.Rbf(xx,yy,z_n,function='cubic')
            f_a = interp.Rbf(xx,yy,z_a,function='cubic')
            f_b = interp.Rbf(xx,yy,z_b,function='cubic')
            f_temperature = interp.Rbf(xx,yy,z_temperature,function='cubic')        
            n = f_n(pressure,condition)
            a = f_a(pressure,condition)
            b = f_b(pressure,condition)
            temperature = f_temperature(pressure,condition)
            logging.info(u'\t\u2714\t\t\u25CF Obtained interpolated exponent ' + \
                'and coefficients for correlation.')
        else:
            logging.info(u'\t\u2714\t\t\u25CF Performing interpolation for ' + \
                'exponent and coefficients')
            x_pressure = coefficients_k_negative_one['pressure']
            y_condition = np.array([1.0,0.5,0.0,-0.5,-1.0])
            xx, yy = np.meshgrid(x_pressure,y_condition)
            z_n = np.vstack((coefficients_mon['n'][-2:], \
                coefficients_aki['n'][-2:],coefficients_ron['n'][-2:],\
                coefficients_k_negative_half['n'][-2:], \
                coefficients_k_negative_one['n'][-2:]))
            z_a = np.vstack((coefficients_mon['a'][-2:],coefficients_aki['a'][-2:],
                coefficients_ron['a'][-2:],coefficients_k_negative_half['a'][-2:], \
                coefficients_k_negative_one['a'][-2:]))
            z_b = np.vstack((coefficients_mon['b'][-2:],coefficients_aki['b'][-2:],
                coefficients_ron['b'][-2:],coefficients_k_negative_half['b'][-2:], \
                coefficients_k_negative_one['b'][-2:]))
            z_temperature = np.vstack((coefficients_mon['temperature'][-2:], \
                coefficients_aki['temperature'][-2:], \
                coefficients_ron['temperature'][-2:], \
                coefficients_k_negative_half['temperature'][-2:],\
                coefficients_k_negative_one['temperature'][-2:]))      
            f_n = interp.Rbf(xx,yy,z_n,function='cubic')
            f_a = interp.Rbf(xx,yy,z_a,function='cubic')
            f_b = interp.Rbf(xx,yy,z_b,function='cubic')
            f_temperature = interp.Rbf(xx,yy,z_temperature,function='cubic')        
            n = f_n(pressure,condition)
            a = f_a(pressure,condition)
            b = f_b(pressure,condition)
            temperature = f_temperature(pressure,condition)
            logging.info(u'\t\u2714\t\t\u25CF Obtained interpolated exponent ' + \
                'and coefficients for correlation.')        


#------------------------------------------------------------------------------------
# Obtain ignition delay time
#------------------------------------------------------------------------------------
    logging.info(u'\t\u2714\t\t\u25CF Obtain ignition delay time.')
    while True:
        ignition_delay_time = input('Enter ignition delay time at T = ' + \
            str(int(temperature)) + ' K (in ms): ')
        try:
            val = float(ignition_delay_time)
            logging.info(u'\t\u2714\t\t\t\u25C6 Ignition delay time input a ' + \
                'numerical value.')
            if float(ignition_delay_time) > 0:
                logging.info(u'\t\u2714\t\t\t\t\u27A4 Ignition delay time ' + \
                    'input a positive value.')
                break
            else:
                logging.warning(u'\t\u2716\t\t\t\t\u27A4 Ignition delay time ' + \
                    'input not a positive value.')
                print u'Please enter non-negative value for ignition delay time'
        except ValueError:
            logging.warning(u'\t\u2716\t\t\t\u25C6 Ignition delay time input ' + \
                'not a numerical value.')
            print 'Please enter a numerical value.'
    ignition_delay_time = float(ignition_delay_time)
    logging.info(u'\t\u2714\t\t\u25CF Obtained ignition delay time.')


#------------------------------------------------------------------------------------
# Calculate ON/OI and display results
#------------------------------------------------------------------------------------
    oi = a * ((ignition_delay_time) ** n) + b
    logging.info(u'\t\u2714\t\t\u25CF Obtained predicted ON/OI.')
    print '---------\n Results\n---------'
    print result_output + str(round(oi,1))


#------------------------------------------------------------------------------------
# Run code again
#------------------------------------------------------------------------------------    
    restart = input('Run code again (y/n)?: ').lower()
    while restart not in ('y','n'):
        logging.warning(u'\t\u2716\t\t\t\t\u27A4 Wrong input provided.')
        sys.stdout.write('Please respond with ''Y/y'' or ''N/n''\n')
        restart = input('Run code again (y/n)?: ').lower()
    if restart == 'y':
        logging.info(u'\t\u2714\t\u229D Running code again.')
        print '====================\n Running code again\n===================='
    elif restart == 'n':
        print 'Exiting'
logging.info(u'\t\u2714\t\t\u263B Normal exit')
time_end = timeit.default_timer()
logging.info(u'\t\u2714\t\t\u231B Finished in ' + \
    str(round(time_end - time_start, 2)) + ' s')